package b_intermediarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Crie um programa que recebe um todo list e a escreve em um arquivo txt.
 * 
 * A cada iteração, o sistema deve pedir uma  única tarefa e exibir a possibilidade
 * de encerrar o programa.
 * 
 * Quando o usuário encerrar o programa, deve-se gerar o arquivo txt com as tarefas
 * que foram inseridas.
 *
 */
public class ListaTarefas {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String menu = "";
		StringBuilder lista = new StringBuilder();
		
		while(! menu.equals("0") ) {
			System.out.println("Digite uma tarefa ou 0 para sair");
			menu = scanner.nextLine();
			
			if(!menu.equals("0")) {
				lista.append(menu + "\n");
			}
		}
		
		Path caminho = Paths.get("teste.txt");
		
		try {
			byte[] listaBytes = lista.toString().getBytes();
			Files.write(caminho, listaBytes);
		} catch (IOException e) {
			System.out.println("Erro ao gravar arquivo!");
		}
		
		scanner.close();
	}
}
