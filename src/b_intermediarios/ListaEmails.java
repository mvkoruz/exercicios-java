package b_intermediarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/**
 * Crie um programa que lê os clientes presentes no arquivo contas.csv e 
 * filtre aqueles que possuem o saldo superior à 7000.
 * Desses clientes, criar outro arquivo de texto que possua todos os clientes
 * no seguinte formato:
 * 
 * João da Silva<joaosilva@teste.com>, Maria da Penha<maria@teste.com>
 * 
 */
public class ListaEmails {
	public static void main(String[] args) {

		int conta = capturarEnt("Digite a conta: ");
		int valor = capturarEnt("Digite o valor do saque: ");

		List<String> lista = lerContas();

		if(lista == null) {
			System.out.println("Não foi possível ler o arquivo");
			return;
		}

		StringBuilder emails = new StringBuilder();
		int count = 0;
		String email;
		for(String linha: lista) {
			email = linha + "\n";
			if(count != 0) {
				String[] partes = linha.split(",");

				int saldo = Integer.parseInt(partes[4]);
				int contaArq = Integer.parseInt(partes[0]);

				if(conta == contaArq) {
					if(saldo > valor) {
						saldo = saldo - valor;
					}
					else {
						System.out.println("O saldo atual é: " + saldo + "\n" + "Não é possível fazer a retirada.");
					}
				}
				email = String.format("%s,%s,%s,%s,%s\n", partes[0], partes[1], partes[2], partes[3], saldo);
			}
			emails.append(email);
			count++;
		}
		gravarArquivo(emails);

	}

	public static List<String> lerContas(){
		Path path = Paths.get("src/contas.csv");

		List<String> lista;

		try {
			lista = Files.readAllLines(path);
		} catch (IOException e) {
			return null;
		}

		return lista;
	}

	public static void gravarArquivo(StringBuilder emails) {
		Path outputPath = Paths.get("mails.txt");

		try {
			Files.write(outputPath, emails.toString().getBytes());
		} catch (IOException e) {
			System.out.println("Não foi possível gravar o arquivo");
		}
	}

	public static int capturarEnt(String texto) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(texto);
		return scanner.nextInt();
	}
}
